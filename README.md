# Сервис актуальных данных

JS course project

## MVP1
- авторизация
- главная страница (просмотр последних сохраненных данных)
- кнопка запроса актуальных данных (автоматическое сохранение в БД)
- форма запроса из базы за период

## Установка

```shell script
npm install
```

## Запуск

```shell script
npm start
```

