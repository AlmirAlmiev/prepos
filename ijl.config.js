const pkg = require('./package')

module.exports = {
    apiPath: "stubs/api",
    webpackConfig: {
        "output": {
            "publicPath": `/static/prepos/${pkg.version}/`
        }
    },
}
